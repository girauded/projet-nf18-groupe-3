#!/usr/bin/python3

import os
import psycopg2

# Greeter is a terminal application that greets old friends warmly,
#   and remembers new friends.

############# VARIABLES GLOBALES ################
# Informations de connexion
HOST = "tuxa.sme.utc"
USER = ""
PASSWORD = ""
DATABASE = ""

# Se connecter à la bdd
dbConnection = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
# Créer un curseur pour executer des requêtes
dbCursor = dbConnection.cursor()


############# FONCTIONS D'ACCES A LA BDD ################
def DB_close_connection():
    dbCursor.close()
    dbConnection.close()

def DB_simple_search():  # Il faut corriger la requete
    # Saisir le titre a chercher
    titre = input("\n Saisissez votre recherche (titre): " )
    # Construire la requête avec ses parametres
    requete = "SELECT titre, date_app, code_class,editeur, prix, genre FROM ressource WHERE ressource.titre = %s "  # Pour ajouter des parametres utiliser le symbole %s à la place de chaque parametre
    parametres = (titre)  # les parametres doivent êtres données dans un tuple python
    # Executer la requete
    dbCursor.execute(requete,parametres)
    # Afficher le resultat
    resultat = dbCursor.fetchone()  # récupère un tuple
    if(resultat) :
        d = 1
        print("\n Resultats :\n")
        while (resultat):
            print(" " + str(d) + " -\tTitre : " + resultat[0])
            print("\tDate Apparition : " + resultat[1] + "\n\n")
            print("\tCode Classement : " + resultat[2] + "\n\n")
            print("\tEditeur : " + resultat[3] + "\n\n")
            print("\tPrix: " + resultat[4] + "\n\n")
            print("\tGenre : " + resultat[5] + "\n\n")
            resultat = dbCursor.fetchone()
            d = d+1
    else :
        print(" Aucun resultat ne correspond a votre recherche !\n")


def DB_advanced_search():
  # Les attributs pour la recherche avancée 
    titre = input ("\n Saisissez le titre : ")
    genre = input ("\n Saisissez le genre : ")
    date_app = input ("\n Saisissez la date d'apparition : ")
    editeur = input ("\n Saisissez l'éditeur : ")
    prix = input ("\n Saisissez le titre : ")
  
  # Requete 
		requete = "SELECT titre, genre, date_app,editeur, prix FROM ressource WHERE (ressource.titre = %s) AND (ressource.genre = %s) AND (ressource.date_app = %s) AND (ressource.editeur= %s) AND (ressource.prix = %s)"
		parametre = (titre, genre, date_app, editeur, prix)
  
  #Executer la requete
  	dbCursor.execute(requete,parametre)
  
   # Afficher le resultat
    resultat = dbCursor.fetchone()  # récupère un tuple
    if(resultat) :
        d = 1
        print("\n Resultats :\n")
        while (resultat):
            print(" " + str(d) + " -\tTitre : " + resultat[0])
            print("\tDGenre : " + resultat[1] + "\n\n")
            print("\tDate Apparition : " + resultat[2] + "\n\n")
            print("\tEditeur : " + resultat[3] + "\n\n")
            print("\tPrix: " + resultat[4] + "\n\n")
            resultat = dbCursor.fetchone()
            d = d+1
    else :
        print(" Aucun resultat ne correspond a votre recherche !\n")
  

def DB_nbemprunt_search():
# Saisir le contributeur
    mail = input("\n Saisissez l'adresse mail d'un adhérent : " )
    # Construire la requête avec ses parametres
    requete = "SELECT COUNT(*) FROM adherent A, emprunt E WHERE A.email = %s AND A.email = E.adherent " 
    parametres = (mail) 
    # Executer la requete
    dbCursor.execute(requete,parametres)
    # Afficher le resultat
    resultat = dbCursor.fetchone()  # récupère un tuple
    if(resultat) :
        d = 1
        print("\n Resultats :\n")
        while (resultat):
            print(" -\tNombre d'emprunts : " + resultat[0])
            resultat = dbCursor.fetchone()
            d = d+1
    else :
        print(" Aucun resultat ne correspond a votre recherche !\n")
                            
def DB_blacklisted_search():
  requete = "SELECT email, nom, prenom FROM Adherent WHERE (Adherent.blackliste = TRUE)"
  parametre = (blackliste)
  dbCursor.execute(requete,parametres)
  resultat = dbCursor.fetchone()  # récupère un tuple
    
    if(resultat) :
        d = 1
        print("\n Resultats :\n")
        while (resultat):
            print(" " + str(d) + " -\email : " + resultat[0])
            print("\nom : " + resultat[1] + "\n\n")
            print("\prenom : " + resultat[2] + "\n\n")
            resultat = dbCursor.fetchone()
            d = d+1
    else :
        print(" Aucun resultat ne correspond a votre recherche !\n")
        
def DB_movies_search():
  movie = input("saisis ton film narvalo")
  requete = "SELECT titre, real, langue,longueur, synopsis, prix FROM ressource JOIN Film ON (film.ressource=ressource.code) WHERE (titre=%s)"
  parametres = (titre,real,langue,longueur,synopsis,prix) 
    # Executer la requete
    dbCursor.execute(requete,parametres)
    # Afficher le resultat
    resultat = dbCursor.fetchone()  # récupère un tuple
    if(resultat) :
        d = 1
        print("\n Resultats :\n")
        while (resultat):
            print(" " + str(d) + " -\titre : " + resultat[0])
            print("\realisateur : " + resultat[1] + "\n\n")
            print("\langue : " + resultat[2] + "\n\n")
            print("\longueur : " + resultat[3] + "\n\n")
            print("\synopsis : " + resultat[4] + "\n\n")
            print("\prix : " + resultat[5] + "\n\n")
            resultat = dbCursor.fetchone()
            d = d+1
    else :
        print(" Aucun resultat ne correspond a votre recherche !\n")
  
############# FUNCTIONS : Affichage du menu ################

def display_title_bar():
    # Efface le contenu de l'ecran et affiche une bannière
    os.system('clear')
    print("\t\t**********************************************")
    print("\t\t*        Projet NF18 - POC Python            *")
    print("\t\t**********************************************")
    print("\n\n")

# Fonction pour l'affichage du menu de selection
# Retourne :la valeur saisie par l'utilisateur
def get_user_choice():

    print(" [1] Recherche simple par titre.") # Recherche par titre
    print(" [2] Recherche multi-critères.") # Recherche Multi-critères
		print(" [3] Nombre d'emprunts pour un utilisateur.") 
    print(" [4] Liste des personnes blacklistées.")
    print(" [5] liste des films disponibles")
    print(" [q] Quit.")
    print("\n")
    return input(" Saisisez votre choix : ")
  


### MAIN ###
def main():
    # Boucle principale
    choice = ''
    while choice != 'q':
        # Recuperer le choix utlisateur
        display_title_bar()
        choice = get_user_choice()
        # Executer la foncion demandée
        if choice == '1':
            DB_simple_search()
        elif choice == '2':
            DB_advanced_search()
		# Rajouter le reste des choix ici en utilisant des elif
        elif choice == '3':
            DB_nbemprunt_search()
        elif choice == '4':
            DB_blacklisted_search()
        elif choice == '5': 
          	DB_movies_search()
      	elif choice == 'q':
            DB_close_connection()
            os.system('clear')
            return 0;
        else:
            continue

        input("\n\n Appuyez sur entrée pour continuer.")
        os.system('clear')


if __name__ == "__main__":
   main()
